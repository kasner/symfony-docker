Symfony Docker integration
==============
<p align="center"><a href="https://symfony.com" target="_blank">
    <img alt="symfony logo" src="https://symfony.com/logos/symfony_black_02.svg"></a>
    <img alt="docker compose logo" src="https://github.com/docker/compose/blob/master/logo.png?raw=true">
</p>

Requirements
------------
* [Git][1]
* [Docker][2]
* [Docker Compose][3]

Installation
------------
1. Clone this repository

```console
git clone git@gitlab.com:kasner/symfony-docker.git <YOUR_APP_NAME>
```

2. go to project directory
```console
cd <YOUR_APP_NAME>
```

3. Remove This origin
```console
git remote rm origin
```

4. Add Your origin
```console
git remote add origin <YOUR_GIT_ADDRESS>
```
5. Add execute permission for run scrip
```console
chmod +X symfony
``` 
Usage
------------
To Start symfony use command:
```console
./symfony start
```
> First initialization can take a while, so You could get to a cafe ;)
> then You can visit [localhost][4]

To stop Symfony use command:
```console
./symfony stop
```

[1]: https://git-scm.com/downloads
[2]: https://docs.docker.com/engine/install/
[3]: https://docs.docker.com/compose/install/
[4]: http://127.0.0.1/